import React from "react";
import {Container, Grid} from 'semantic-ui-react'

const articleCardStyle = {
    'marginBottom' : '15px',
    'border' : '1px solid black'
}

const cutContext = (context) => {
    const maxLength = 540;

    if(context.length <= 200){
        return context;
    }

    context = context.substring(0, maxLength);
    let last = context.lastIndexOf(" ");
    context = context.substring(0, last);
    return context + " ..."
}

const ArticleCard = (props) => {
    const infoCard = () => {
        return(
                <Grid columns={2} padded>
                    <Grid.Row color={'black'}>
                        <Grid.Column width={12}>
                            {props.article['name']}
                        </Grid.Column>
                        <Grid.Column width={4}>
                            {props.article['createdTime']}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
        );
    };

    return(
        <Container style={articleCardStyle}>
            {infoCard()}
            <Grid columns={1} padded>
                <Grid.Column width={16}>
                    {cutContext(props.article['originalText'])}
                </Grid.Column>
            </Grid>
        </Container>
    );
};

export default ArticleCard;