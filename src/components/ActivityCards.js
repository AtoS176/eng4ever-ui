import React from "react";
import {Link} from 'react-router-dom';
import {Card, Container, Header} from 'semantic-ui-react'


const images = [
    '/images/article.jpg',
    '/images/image.jpg',
    '/images/movie.jpg'
];

const descriptions = [
    {
        header: 'Artykuły',
        description: 'Tłumaczenie z angielskiego na polski.',
        link: '/article'
    }, {
        header: 'Zdjęcia',
        description: 'Opisywanie Zdjęcia',
        link: '/image'
    }, {
        header: 'Filmy',
        description: 'Ćwiczenia ze suchania',
        link: '/movie'
    }
]

const containerStyle = {
    'margin': '100px 50px 50px 50px',
}

const headerStyle = {
    'margin': '30px 30px 35px 30px'
}

const cardStyle = {
    'marginLeft' : '20px',
    'marginBottom' : '20px'
}

const color = 'blue';


const ActivityCards = () => {
    const activityCard = (number) => {
        return (
            <Link to={descriptions[number]['link']}>
                <Card
                    style={cardStyle}
                    color={color}
                    image={images[number]}
                    header={descriptions[number]['header']}
                    description={descriptions[number]['description']}
                />
            </Link>
        )
    }

    return (
        <Container style={containerStyle}>
            <Header
                style={headerStyle}
                size='huge'
                color={color}>
                Wybierz Rodzaj Aktywności
            </Header>
            <Card.Group itemsPerRow={3} textAlign='center'>
                {activityCard(0)}
                {activityCard(1)}
                {activityCard(2)}
            </Card.Group>
        </Container>
    );
}

export default ActivityCards;