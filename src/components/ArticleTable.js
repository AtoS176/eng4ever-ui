import React, {useState, useEffect} from "react";
import axios from 'axios';
import {Container} from 'semantic-ui-react'
import ArticleCard from "./ArticleCard";
import ArticleAdder from "./ArticleAdder";

const containerStyle = {
    'margin': '150px 50px 50px 50px',
}

const ArticleTable = () => {
    const url = 'http://localhost:8080/article/find?userId=1';
    const [data, setData] = useState([]);

    useEffect(() => {
        axios.get(url).then(json => setData(json.data))
    }, []);

    const renderTable = () => {
        return data.map(article => {
            return (<ArticleCard key={article['id']} article={article}/>)
        })
    };

    return (
        <Container style={containerStyle}>
            <ArticleAdder/>
            {renderTable()}
        </Container>
    );
}

export default ArticleTable;