import React from "react";
import axios from 'axios';
import {Form, Button, Container, Header} from 'semantic-ui-react'

const containerStyle = {
    'marginBottom' : '30px',
    'border' : '1px solid black',
    'borderRadius' : '25px',
    'padding' : '20px'
}

class ArticleAdder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            context: ''
        }
    }

    onSubmit = (evt) => {
        evt.preventDefault();

        const body = {
            'name' : this.state.name,
            'originalText' : this.state.context
        };

        const url = 'http://localhost:8080/article/save';

        axios.post(url, body)
            .then(json => {
                console.log(json.data)
                if(json.data === true) {
                    window.location.reload(false);
                }
            })
    };

    render() {
        return(
            <Container style={containerStyle}>
                <Header>Dodaj nowy artykuł</Header>
                <Form onSubmit={this.onSubmit}>
                    <Form.Field onChange={e => this.setState({name : e.target.value})}>
                        <label>Nazwa Artykułu</label>
                        <input placeholder='Nazwa Artykułu' />
                    </Form.Field>
                    <Form.Field onChange={e => this.setState({context : e.target.value})}>
                        <label>Tekst</label>
                        <textarea/>
                    </Form.Field>
                    <Button type='submit'>Dodaj</Button>
                </Form>
            </Container>
        )
    };

}

export default ArticleAdder;