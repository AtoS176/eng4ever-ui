import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ActivityCards from './components/ActivityCards';
import ArticleTable from './components/ArticleTable';
import 'semantic-ui-css/semantic.min.css'

ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path="/" component={ActivityCards}/>
            <Route exact path="/article" component={ArticleTable}/>
        </Switch>
    </Router>,
    document.getElementById('root')
);
